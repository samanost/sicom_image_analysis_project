import numpy as np
import cv2
from scipy.signal import convolve2d

from src.forward_model import CFA


def superpixel(op: CFA, y: np.ndarray) -> np.ndarray:
    """Performs the method of variable number of gradients

    Args:
        op (CFA): CFA operator.
        y (np.ndarray): Mosaicked image.

    Returns:
        np.ndarray: Demosaicked image.
    """
    z = op.adjoint(y)

    if op.cfa == 'bayer':
        res = np.empty((op.input_shape[0]//2, op.input_shape[1]//2, op.input_shape[2]))

        for i in range(1,op.input_shape[0],2):
            for j in range(1, op.input_shape[1],2):
                res[i//2,j//2,0] = z[ i-1,j ,0]
                res[i//2,j//2,1] = (z[i,j,1] + z[i-1,j-1,1]) / 2
                res[i//2,j//2,2] = z[ i,j-1 ,2]

    else:
        res = np.empty((op.input_shape[0]//2, op.input_shape[1]//2, op.input_shape[2]))

        print()

        for i in range(2,op.input_shape[0]-5,4):
            for j in range(2, op.input_shape[1]-5,4):

                res[i//2,j//2,0] = z[ i-2,j ,0]
                res[i//2,j//2,1] = (z[i,j,1] + z[i-2,j-2,1]) / 2
                res[i//2,j//2,2] = z[ i,j-2 ,2]

                res[i//2+1,j//2,0] = z[ i-2+1,j ,0]
                res[i//2+1,j//2,1] = (z[i+1,j,1] + z[i-2+1,j-2,1]) / 2
                res[i//2+1,j//2,2] = z[ i+1,j-2 ,2]

                res[i//2+1,j//2+1,0] = z[ i-2+1,j+1 ,0]
                res[i//2+1,j//2+1,1] = (z[i+1,j+1,1] + z[i-2+1,j-2+1,1]) / 2
                res[i//2+1,j//2+1,2] = z[ i+1,j-2+1 ,2]

                res[i//2,j//2+1,0] = z[ i-2,j+1 ,0]
                res[i//2,j//2+1,1] = (z[i,j+1,1] + z[i-2,j-2+1,1]) / 2
                res[i//2,j//2+1,2] = z[ i,j-2+1 ,2]

    return cv2.resize(res, (op.input_shape[0], op.input_shape[1]))


####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
