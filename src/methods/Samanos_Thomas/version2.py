import numpy as np
from scipy.signal import convolve2d

CONV2 = np.array([[1/4,1/2,1/4],[1/2,1,1/2],[1/4,1/2,1/4]])

def bayer_reconstruction(y : np.ndarray, input_shape : tuple) -> np.ndarray:
    op_shape = (input_shape[0],input_shape[1],3)
    op = np.zeros(op_shape)
    conv = np.zeros((2,2,3))
    liste = [(0,1,0),(0,0,1),(1,1,1),(1,0,2)]
    for el in liste :
        conv[el]=1
    op = masq(y,conv, 2)
    op1 = np.zeros_like(op)
    for color in range(3):
        op1[:,:,color] = convolve2d(op[:,:,color],CONV2,mode ="same")
        if color == 1 :
            op1[:,:,color] = op1[:,:,color]/2
    return op1

CONV4 = np.array([[11/156,43/312,2/13,43/312,11/156],
                 [43/312,1/6,1/4,1/6,43/312],
                 [2/13,1/4,1/3,1/4,2/13],
                 [43/312,1/6,1/4,1/6,43/312],
                 [11/156,43/312,2/13,43/312,11/156]])

def quad_bayer_reconstruction(y : np.ndarray, input_shape : tuple) -> np.ndarray:
    op_shape = (input_shape[0],input_shape[1],3)
    op = np.zeros(op_shape)
    conv = np.zeros((4,4,3))
    liste = [(2,0,2),(3,0,2),(2,1,2),(3,1,2),(0,0,1),(1,0,1),(0,1,1),(1,1,1),(2,2,1),(2,3,1),(3,2,1),(3,3,1),(0,2,0),(0,3,0),(1,2,0),(1,3,0)]
    for el in liste :
        conv[el]=1
    op = masq(y,conv, 4)
    op1 = np.zeros_like(op)
    for color in range(3):
        op1[:,:,color] = convolve2d(op[:,:,color],CONV4,mode ="same")
        if color == 1 :
            op1[:,:,color] = op1[:,:,color]/2
    return op1

def masq(img : np.ndarray,conv:np.ndarray, pas:int) ->np.ndarray:
    masq = np.zeros((img.shape[0],img.shape[1],3))
    po = np.zeros((img.shape[0],img.shape[1],3))
    for color in range (3):
        for i in range(0,po.shape[0],pas):
            for j in range(0,po.shape[1],pas):
                masq[i:i+pas,j:j+pas,color] = conv[:,:,color]
        po[:,:,color] = masq[:,:,color] * img
    return po