import numpy as np
from .clip_values import clip_values
from .process_channel import process_channel
from .process_interpolation import process_interpolation

def process_red_channel(img: np.ndarray, N: int, M: int) -> np.ndarray:
    """Process the red channel of an image
    
    Args:
        img (np.ndarray): image to process
        N (int): height of the image
        M (int): width of the image
        
    Returns:
        np.ndarray: processed image
    """
    
    img = process_interpolation(img, 0, N, M)   
    img = process_channel(img, 0, N, M)               
    img = clip_values(img)
    
    return img