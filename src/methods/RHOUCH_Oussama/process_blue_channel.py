import numpy as np
from .find_neighbors import find_neighbors
from .find_direct_neighbors import find_direct_neighbors
from .find_weights import find_weights
from .interpolate import interpolate
from .interpolate_neighboring import interpolate_neighboring
from .clip_values import clip_values
from .process_channel import process_channel
from .process_interpolation import process_interpolation

def process_blue_channel(img: np.ndarray, N: int, M: int) -> np.ndarray:
    """
    Process the blue channel of an image
    
    Args:
        img (np.ndarray): image to process
        N (int): height of the image
        M (int): width of the image
        
    Returns:
        np.ndarray: processed blue channel of the image
    """
    
    img = process_interpolation(img, 2, N, M)
    img = process_channel(img, 2, N, M)
    img = clip_values(img)
    
    return img