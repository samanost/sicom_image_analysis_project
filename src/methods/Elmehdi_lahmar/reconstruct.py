"""The main file for the reconstruction.
This file should NOT be modified except the body of the 'run_reconstruction' function.
Students can call their functions (declared in others files of src/methods/your_name).
"""


import numpy as np
from src.methods.Elmehdi_lahmar.malvar import malvar
from src.forward_model import CFA

def run_reconstruction(y: np.ndarray, cfa: str) -> np.ndarray:
    """
    Run the demosaicing process using Malvar-He-Cutler algorithm for Bayer pattern.

    Args:
    y (np.ndarray): The mosaicked image to be reconstructed.
    cfa (str): Name of the CFA, expected to be 'bayer'.

    Returns:
    np.ndarray: The demosaicked image.
    """
    if cfa != 'bayer':
        raise ValueError("Malvar-He-Cutler demosaicing only supports Bayer CFA pattern.")

    input_shape = (y.shape[0], y.shape[1], 3)
    op = CFA(cfa, input_shape)

    return malvar(y, op)






####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
