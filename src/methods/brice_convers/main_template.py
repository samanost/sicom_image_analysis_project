import src.methods.brice_convers.dataHandler as DataHandler
import src.methods.brice_convers.dataEvaluation as DataEvaluation
import time

WORKING_DIRECOTRY_PATH = "SICOM_Image_Analysis/sicom_image_analysis_project/"

DataHandler = DataHandler.DataHandler(WORKING_DIRECOTRY_PATH)
DataEvaluation = DataEvaluation.DataEvaluation(DataHandler)

def main(DataHandler):

    IMAGE_PATH = WORKING_DIRECOTRY_PATH + "images/"

    CFA_NAME = "quad_bayer"

    METHOD = "menon"

    startTime = time.time()

    DataHandler.list_images(IMAGE_PATH)

    DataHandler.print_list_images()

    DataHandler.compute_CFA_images(CFA_NAME)

    DataHandler.compute_reconstruction_images(METHOD, {"cfa": CFA_NAME})

    DataHandler.plot_reconstructed_image(0, METHOD, {"cfa": CFA_NAME}, zoomSize="large")

    DataEvaluation.print_metrics(0, METHOD)

    endTime = time.time()

    print("[INFO] Elapsed time: " + str(endTime - startTime) + "s")

    print("[INFO] End")

if __name__ == "__main__":
    main(DataHandler)
